terraform {
  required_version = ">= 0.12, < 0.13"
  backend "s3" {}
}

provider "aws" {
  version = "~> 2.8"
  region  = var.region
}

resource "aws_route53_zone" "primary" {
  name = var.primary_domain

  lifecycle {
    prevent_destroy = true
  }

  tags = {
    Environment = var.environment
    Product     = var.product
  }
}
