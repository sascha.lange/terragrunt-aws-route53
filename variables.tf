variable "region" {
    description = "Region for resources"
    type = string
}

variable "environment" {
    description = "Environment type"
    type        = string
}

variable "product" {
    description = "Product name"
    type        = string
}

variable "primary_domain" {
    description = "Primary domain"
    type        = string
}

