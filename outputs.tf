output "primary_name_servers" {
    value = aws_route53_zone.primary.name_servers
}

output "primary_zone_id" {
    value = aws_route53_zone.primary.zone_id
}
